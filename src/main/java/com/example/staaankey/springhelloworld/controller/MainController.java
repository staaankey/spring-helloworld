package com.example.staaankey.springhelloworld.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@ResponseStatus(HttpStatus.CREATED)
class MainController {
    @PostMapping("/sayHello/{name}")
    public ResponseEntity<String> postHelloTo(@PathVariable String name) {
        return new ResponseEntity<>("Hello to: " + name, HttpStatus.OK);
    }
}